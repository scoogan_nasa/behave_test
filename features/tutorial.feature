Feature: showing off behave

@all
Scenario: run a simple test
    Given we have behave installed
    When we implement a test
    Then behave will test it for us!

@all @blah
Scenario: Some other random test
    Given we have behave installed
    When we implement some other test
    Then behave will test it for us!

@nested
Scenario: Some verbosely worded test
    Given some given
    When I do step 1
    And I do step 2
    And I do step 3
    Then I expect some then

@nested
Scenario: Shorter version of verbosely worded test
    Given some given
    When I do all three steps
    Then I expect some then