@given(u'some given')
def step_impl(context):
    print("Some given")

@when(u'I do step 1')
def step_impl(context):
    print("step 1")

@when(u'I do step 2')
def step_impl(context):
    print("step 2")

@step(u'I do step 3')
def step_impl(context):
    print("step 3")

@then(u'I expect some then')
def step_impl(context):
    print("some then")

@when(u'I do all three steps')
def step_impl(context):
    context.execute_steps('''
        when I do step 1
        and I do step 2
        when I do step 3
    ''')
