from behave import *

@given('we have behave installed')
def step_impl(context):
    pass

@when('we implement a test')
def step_impl(context):
    assert True is not False

@when('we implement some {:w} test')
def step_impl(context, blah):
    print("other test")
    assert True is not False

@step('behave will test it for us!')
def step_impl(context):
    assert context.failed is False